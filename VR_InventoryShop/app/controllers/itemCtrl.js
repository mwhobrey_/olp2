﻿angular.module('app')
 .controller('itemCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', '$cookieStore', '$state', function ($scope, $rootScope, $location, idealsApi, $cookieStore, $state) {
     try {

         // respinse of list_inventory method
             var list_inventoryResponse = JSON.parse(sessionStorage['list_inventory'] || '{}');
             var json2 = list_inventoryResponse;
          
             $scope.inventoryList = json2.data.v1.items;
             var inventoryList = json2.data.v1.items;



         // click on new item to select all new items only
             $scope.ClickNewItem = function ($event) {
                 $scope.inventoryList = [];
                 if($event==true)
                 {
                   
                     for (var i = 0; i <= (inventoryList).length - 1; i++)
                     {
                         if (inventoryList[i].isNew == true)
                         {                           
                             $scope.inventoryList.push(inventoryList[i]);
                         }
                     }

                 }
                 else
                 {
                     for (var i = 0; i <= (inventoryList).length - 1; i++) {
                       
                         $scope.inventoryList.push(inventoryList[i]);
                       
                     }
                 }
             }



         // range select for item
             $scope.selectRange = function (selectedRange)
             {
                 if(selectedRange=="Cash Price Low to High")
                 {
                     $scope.filter='price';
                 }
                 else if(selectedRange=="Cash Price High to Low")
                 {
                     $scope.filter='-price';
                 }
                 else if(selectedRange=="Weekly Rate Low to High")
                 {
                     $scope.filter='weeklyRate';
                 }
                 else if(selectedRange=="Weekly Rate High to Low")
                 {
                     $scope.filter='-weeklyRate';
                 }
                 else if(selectedRange=="Monthly Rate Low to High")
                 {
                     $scope.filter='monthlyRate';
                 }
                 else if(selectedRange=="Monthly Rate High to Low")
                 {
                     $scope.filter='-monthlyRate';
                 }
                 
             }


             $scope.selectitem=function(data)
             {
                 $scope.cardUpdate(data);

             }

     }
     catch(e)
     {
         console.log(e);
     }
 }]);