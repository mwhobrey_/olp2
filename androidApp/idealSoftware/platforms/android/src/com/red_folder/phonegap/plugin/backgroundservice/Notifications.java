package com.red_folder.phonegap.plugin.backgroundservice;

/**
 * Created by seemag on 12/10/2017.
 */

public class Notifications {



    //private variables
    int _id;
    String _deviceID;
    String _msgReq;
    String _msgResp;
    String _dateTime;
    String _token;
    String _clientID;
    int _storeID;
    int _count;
    String _api;
    String _group;
    String _privateKey;
    String _apiToken;
    String _clientUrl;

    // Empty constructor
    public Notifications(){

    }
    // constructor
    public Notifications(int id,String deviceID, String msgReq, String msgResp,String dateTime,String token,int storeID,String clientID ){
        this._id = id;
        this._deviceID = deviceID;
        this._msgReq = msgReq;
        this._msgResp = msgResp;
        this._dateTime = dateTime;
        this._token = token;
        this._clientID=clientID;
        this._storeID=storeID;
    }

    // constructor
    public Notifications(String deviceID, String msgReq, String msgResp,String dateTime,String token,int storeID,String clientID){
        this._deviceID = deviceID;
        this._msgReq = msgReq;
        this._msgResp = msgResp;
        this._dateTime = dateTime;
        this._token = token;
        this._clientID=clientID;
        this._storeID=storeID;
    }
    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getDeviceID(){
        return this._deviceID;
    }

    // setting name
    public void setDeviceID(String deviceID){
        this._deviceID = deviceID;
    }

    // getting phone number
    public String getmsgReq(){return this._msgReq;}

    // setting phone number
    public void setmsgReq(String msgReq){
        this._msgReq = msgReq;
    }

    // getting phone number
    public String getmsgResp(){return this._msgResp;}

    // setting phone number
    public void setmsgResp(String msgResp){
        this._msgResp = msgResp;
    }

    // getting phone number
    public String getDateTime(){return this._dateTime;}

    // setting phone number
    public void setDateTime(String dateTime){
        this._dateTime = dateTime;
    }

    // getting phone number
    public String getToken(){return this._token;}

    // setting phone number
    public void setToken(String token){
        this._token = token;
    }

    // getting phone number
    public String getClientID(){return this._clientID;}

    // setting phone number
    public void setClientID(String clientID){
        this._clientID = clientID;
    }

    public int getStoreID(){return this._storeID;}

    // setting phone number
    public void setStoreID(int storeID){
        this._storeID = storeID;
    }


    public int getcount(){return this._count;}

    // setting phone number
    public void setcount(int count){
        this._count = count;
    }


    public String getapi(){return this._api;}

    // setting phone number
    public void setapi(String api){
        this._api = api;
    }



    public String getgroup(){return this._group;}

    // setting phone number
    public void setgroup(String group){
        this._group = group;
    }


    public String getprivateKey(){return this._privateKey;}

    // setting phone number
    public void setprivateKey(String privateKey){
        this._privateKey = privateKey;
    }


    public String getapiToken(){return this._apiToken;}

    // setting phone number
    public void setapiToken(String apiToken){
        this._apiToken = apiToken;
    }

    public String getclientUrl(){return this._clientUrl;}

    // setting phone number
    public void setclientUrl(String clientUrl){
        this._clientUrl = clientUrl;
    }


}
