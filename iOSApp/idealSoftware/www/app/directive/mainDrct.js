﻿
angular.module("app")
    .directive('myTextArea', function() {
      var startingText = "Starting text";
      return {
        restrict: 'AE',
        template: '<div class="input-block text-area-sec"><textarea id="txtArea" class="text-entry" type="textarea"  ng-bind-html="chat" placeholder=""  ng-model="chat"></textarea></div><div class="button-div"><button class="btn pull-right send-btn" ng-click="send(chat)"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>',
        link: function(scope, element, attrs) {
          scope.text = startingText;
          scope.changeText = function() {
            scope.text = '\nLOOP 100\n  WAIT 5\n  input hello\n  if 1\n    message\n  endif\nENDLOOP\nHANGUP\n\n\n\u0000';
          }
        }
      };
    });