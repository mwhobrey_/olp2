angular.module("app")
    .controller("errorMsgCtrl", ["$scope", "$window", "configDetailsProvider","SharedService", function($scope, $window, configDetailsProvider,SharedService) {
        try {

  
            $rootScope.IsShopShow = configDetailsProvider.apiConnect.IsShopShow;
            document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);
                                 
             $scope.start = function() {
             //$("#nav-icon").click(function (e) {
             $(".sidebar").toggleClass("open");
             //});
             }

            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "paymentApp/index.html";
            };
            $scope.about = function() {
                $location.path("/about");
            }


            $(".navbar-bg").css("background", configDetailsProvider.apiConnect.color);
            $(".sidebar").css("background", configDetailsProvider.apiConnect.color);
            $("#nav-icon").css("background", configDetailsProvider.apiConnect.color);
            $(".menu").css("background", configDetailsProvider.apiConnect.color);

            if (configDetailsProvider.apiConnect.flag == false) {
                $(".ShopDisabled").css('pointer-events', 'none');
                $(".ShopDisabled").css('opacity', '0.6');
            }


        } catch (e) {
            console.log(e);
        }
    }]);
