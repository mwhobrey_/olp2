﻿angular.module('app')
 .controller('paymentDeclineCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', function ($scope, $rootScope, $location, idealsApi) {
     try
     {
         // redirect to checkout page
         //$rootScope.message = "";
         $scope.clickCheckout = function () {
         $rootScope.message = "";
         $location.path('/checkOut');

         };
         //logout click
         $scope.logoutClick = function () {   
             idealsApi.logoutReq();
         };
     }
     catch(e)
     {
         console.log(e);
     }

}]);